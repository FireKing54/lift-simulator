/**
 * A priori, cette classe ne doit pas être modifiée. C'est possible quand même.
 * À vous de juger !
 */
public abstract class Evenement extends Global {

	/**
	 * en dixième de secondes
	 */
	protected long date;

	public Evenement(long d) {
		assert d >= 0;
		date = d;
	}

	public abstract void traiter(Immeuble immeuble, Echeancier echeancier);

	public void affiche(StringBuilder buffer, Immeuble immeuble) {
		buffer.append("[" + date + ",");
		this.afficheDetails(buffer, immeuble);
		buffer.append("]");
	}

	public abstract void afficheDetails(StringBuilder buffer, Immeuble immeuble);

}
