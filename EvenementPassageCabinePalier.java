/**
 * PCP: Passage Cabine Palier L'instant précis où la cabine passe juste en face
 * d'un étage précis. Vous pouvez modifier cette classe comme vous voulez
 * (ajouter/modifier des méthodes etc.).
 */
public class EvenementPassageCabinePalier extends Evenement {

	private Etage étage;

	public EvenementPassageCabinePalier(long d, Etage e) {
		super(d);
		étage = e;
	}

	public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
		buffer.append("PCP ");
		buffer.append(étage.numéro());
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		Cabine cabine = immeuble.cabine;
		assert !cabine.porteOuverte;
		assert étage.numéro() != cabine.étage.numéro();
		
		cabine.étage = this.étage;
		
		if (cabine.passagersVeulentDescendre() || étage.aDesPassagers()) {
			echeancier.ajouter(new EvenementOuverturePorteCabine(date + tempsPourOuvrirOuFermerLesPortes));
		}
		else {
			
			int numeroProchainEtage = cabine.étage.numéro();
			if (cabine.intention() == '^') {
				numeroProchainEtage++;
			}
			else if (cabine.intention() == 'v') {
				numeroProchainEtage--;
			}
			
			
			if (numeroProchainEtage != cabine.étage.numéro()) {
				echeancier.ajouter(new EvenementPassageCabinePalier(date + tempsPourBougerLaCabineDUnEtage, immeuble.étage(numeroProchainEtage)));
			}
		}

	}
}
